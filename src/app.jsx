import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import configureStore from './redux/store/configureStore';

import Carousel from './containers/Carousel';
import AppControls from './containers/AppControls';

const { store, persistor } = configureStore();

export default class App extends React.Component {
    render() {
        return (
          <Provider store={store} >
            <PersistGate loading={null} persistor={persistor}>
              <div>
                <AppControls />
                <Carousel />
              </div>
            </PersistGate>
          </Provider>
        );
    }
}
