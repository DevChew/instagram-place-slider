import http from '../common/http';
import { getPostsIdFromResponse, getPostDataFreomResponse } from '../common/responseTransformers';
import endposints, { makePostShortcodeUrl } from '../common/endpoints';
import { setSlides, setSlide } from '../redux/actions/instagramSlidesActions';

export const fetchLatestPostsIds = (dispatch) => {
    http
        .request({
            url: endposints.feedUrl,
        })
        .then(getPostsIdFromResponse)
        .then(slides => dispatch(setSlides(slides)));
};

export const fetchPostById = (dispatch, id) => {
    http
        .request({
            url: makePostShortcodeUrl(id),
        })
        .then(getPostDataFreomResponse)
        .then(slide => dispatch(setSlide(slide)));
};

export default {};
