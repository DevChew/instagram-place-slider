import {
    createStore,
    combineReducers,
} from 'redux';
import {
    persistStore,
    persistReducer,
} from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import instagramSlidesReducer from './../reducers/instagramSlides';

const persistConfig = {
    key: 'root',
    storage,
};

const persistedReducer = persistReducer(
    persistConfig,
    combineReducers({
        instagramSlides: instagramSlidesReducer,
    }),
);

export default () => {
    const store = createStore(persistedReducer);
    const persistor = persistStore(store);
    return {
        store,
        persistor,
    };
};
