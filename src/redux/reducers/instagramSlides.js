import initialStore from '../store/initialInstagramSlidesStore';
import assignment from 'assignment';

const setSlides = (state, action) => assignment(
        {},
        state,
    {
        slides: action.state,
    },
);

const setSlide = (state, action) => {
    const slide = action.state;
    const postData = {
        [slide.id]: slide,
    };
    return assignment(
        {},
        state,
        {
            postData,
        },
    );
};

export default (state = initialStore, action) => {
    const payload = {
        SET_SLIDES: setSlides,
        SET_SLIDE: setSlide,
    };

    return (payload[action.type] || (() => state))(state, action);
};
