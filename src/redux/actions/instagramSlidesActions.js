
export const setSlides = (state = []) => ({
    type: 'SET_SLIDES',
    state,
});

export const setSlide = (state = {}) => ({
    type: 'SET_SLIDE',
    state,
});

export default {};
