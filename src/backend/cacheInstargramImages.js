import {
    session,
} from 'electron';
import os from 'os';
import fs from 'fs';
import request from 'request';

const projectDir = `${os.homedir()}/InstagramSlider`;
const imagesDir = `${projectDir}/images`;

if (!fs.existsSync(projectDir)) {
    fs.mkdirSync(projectDir);
}
if (!fs.existsSync(imagesDir)) {
    fs.mkdirSync(imagesDir);
}

const downloadAndStore = (uri, filename, callback) => {
    request.head(uri, () => {
        request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
    });
};

const checkIfHasLocally = ({ filename, destination }) => {
    console.log({ filename, destination });
    return false;
};

export const cacheInstargramImages = () => {
    const instagramImagesFilter = {
        urls: ['https://*.cdninstagram.com/*'],
    };

    session
        .defaultSession
        .webRequest
        .onBeforeRequest(instagramImagesFilter, (details, callback) => {
            if (details.resourceType === 'image') {
                const filename = details.url.split('/').pop().split('#')[0].split('?')[0];
                const imageInfo = {
                    originalUrl: details.url,
                    filename,
                    destination: `${imagesDir}/${filename}`,
                };

                const hasLocally = checkIfHasLocally(imageInfo);

                if (hasLocally) {
                    console.log('file has local copy, redirect');
                    callback({
                        cancel: false,
                        redirectURL: imageInfo.destination,
                    });
                } else {
                    downloadAndStore(imageInfo.originalUrl, imageInfo.destination, () => {
                        console.log('downloading done');
                    });
                    callback({
                        cancel: false,
                    });
                }
            }
        });
};

export default {};
