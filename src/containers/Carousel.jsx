import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { fetchLatestPostsIds } from '../api/instagram';
import InstagramItem from './../components/InstagramItem';
import settings from '../../settings.json';

export class CarouselElement extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentSlide: 0,
            totalSlides: 0,
            slideStep: 1,
            totalVisibleSlides: settings.maxShownImages,
            interval: settings.secondsPerSlide * 1000,
        };

        setInterval(() => {
            this.nextSlide();
        }, this.state.interval);

        setInterval(() => {
            this.props.actions.update();
        }, settings.updateIntervalInSeconds * 1000);
    }

    componentDidMount() {
        this.props.actions.update();
        this.updateSlidesTotal();
        this.gotoSlide(0);
    }

    componentDidUpdate(prevProps) {
        const { slides } = this.props;
        const prevSlides = prevProps.slides;
        if (JSON.stringify(slides) !== JSON.stringify(prevSlides)) {
            this.updateSlidesTotal();
        }
    }

    gotoSlide(slideNo) {
        this.updateSlidesTotal();
        const { totalSlides } = this.state;

        if (slideNo > totalSlides) {
            this.setState({
                currentSlide: 0,
            });
        } else if (slideNo <= 0) {
            this.setState({
                currentSlide: totalSlides,
            });
        } else {
            this.setState({
                currentSlide: slideNo,
            });
        }
    }

    nextSlide() {
        const { currentSlide, slideStep } = this.state;
        this.gotoSlide(currentSlide + slideStep);
    }

    prevSlide() {
        const { currentSlide, slideStep } = this.state;
        this.gotoSlide(currentSlide - slideStep);
    }

    updateSlidesTotal() {
        const { slides } = this.props;
        this.setState({
            totalSlides: slides.length - 1,
        });
    }

    render() {
        const { slides } = this.props;
        const { currentSlide, totalVisibleSlides } = this.state;
        const posts = slides.map((slide, index) => {

            const isCurrent = index === currentSlide;

            const overLimit =
                (index - currentSlide) > totalVisibleSlides / 2 ||
                (currentSlide - index) > totalVisibleSlides / 2;

            if (overLimit) {
                return null;
            }
            return (
              <div
                className={
                    classNames(
                        'carousel__slide', {
                            'carousel__slide--current': isCurrent,
                            [`carousel__slide--before-${index - currentSlide}`]: index < currentSlide,
                            [`carousel__slide--after-${currentSlide - index}`]: index > currentSlide,
                        },
                    )
                }
                key={slide.id}
              >
                <InstagramItem {...slide} />
              </div>
            );
        });

        return (
          <div className="instagram">
            <div className="carousel">
              <div className="carousel__slides">
                {posts}
              </div>
            </div>
          </div>
        );
    }
}

CarouselElement.propTypes = {
    slides: PropTypes.array,
    actions: PropTypes.object,
};

CarouselElement.defaultProps = {
    slides: [],
    actions: {},
};

const mapStateToProps = state => ({
    slides: state.instagramSlides.slides,
    postData: state.instagramSlides.postData,
});

const mapDispatchToProps = dispatch => ({
    actions: {
        update: () => fetchLatestPostsIds(dispatch),
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(CarouselElement);
