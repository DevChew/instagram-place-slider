import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchPostById } from '../api/instagram';
import InstagramItem from './../components/InstagramItem';

class Slide extends Component {

    componentDidMount() {
        const { actions, id, postData } = this.props;
        if (!postData[id]) {
            actions.getPostData(id);
        }
    }

    render() {
        const { postData, id } = this.props;
        const post = postData[id] || {};

        return <InstagramItem {...post} />;
    }
}

Slide.propTypes = {
    id: PropTypes.string,
    postData: PropTypes.object,
    actions: PropTypes.object,
};

Slide.defaultProps = {
    id: '',
    postData: {},
    actions: {},
};

const mapStateToProps = state => ({
    postData: state.instagramSlides.postData,
});

const mapDispatchToProps = dispatch => ({
    actions: {
        getPostData: id => fetchPostById(dispatch, id),
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(Slide);
