import React, { Component } from 'react';
import { ipcRenderer } from 'electron';

class AppControls extends Component {
    render() {
        const clickOnClose = () => {
            ipcRenderer.send('action-bar', 'close');
        };
        return (
          <div className="app-controls">
            <button className="app-controls__close" onClick={clickOnClose}>X</button>
          </div>
        );
    }
}

export default AppControls;
