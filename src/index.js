import {
    app,
    BrowserWindow,
    powerMonitor,
    ipcMain,
} from 'electron';
import installExtension, {
    REACT_DEVELOPER_TOOLS,
} from 'electron-devtools-installer';
import {
    enableLiveReload,
} from 'electron-compile';
import { cacheInstargramImages } from './backend/cacheInstargramImages';

let mainWindow;
let isSuspend = false;

const isDevMode = process.execPath.match(/[\\/]electron/);

if (isDevMode) {
    enableLiveReload({
        strategy: 'react-hmr',
    });
}

const createMainWindow = async () => {

    const windowOptions = {
        width: 800,
        height: 600
    };

    if (!isDevMode) {
       windowOptions.frame = false;
       windowOptions.fullscreen = true; 
    }
    
    mainWindow = new BrowserWindow(windowOptions);

    if (!isDevMode) {
        mainWindow.maximize();
        mainWindow.setFullScreen(true);
        mainWindow.focus();
        mainWindow.show();
    }

    mainWindow.loadURL(`file://${__dirname}/index.html`);

    if (isDevMode) {
        await installExtension(REACT_DEVELOPER_TOOLS);
        mainWindow.webContents.openDevTools();
    }

    mainWindow.on('closed', () => {
        mainWindow = null;
    });
};

app.on('ready', createMainWindow);
app.on('ready', cacheInstargramImages);

app.on('ready', () => {
    powerMonitor.on('suspend', () => {
        isSuspend = true;
        mainWindow.close();
        mainWindow = null;
        console.log('The system is going suspend');
    });

    powerMonitor.on('resume', () => {
        console.log('The system is resume');
        isSuspend = false;
        if (mainWindow === null) {
            createMainWindow();
        }
    });
});

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        if (!isSuspend) {
            app.quit();
        }
    }
});

app.on('activate', () => {
    if (mainWindow === null) {
        createMainWindow();
    }
});

ipcMain.on('action-bar', (event, arg) => {
    if (arg === 'close') {
        app.quit();
    }
});
