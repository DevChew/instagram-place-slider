export const makePostShortcodeUrl = (postShortcode) => (
    `https://www.instagram.com/p/${postShortcode}/?__a=1`
);

export default {
    feedUrl: 'https://www.instagram.com/explore/tags/rynekkawiarniagaleria/?__a=1'
};
