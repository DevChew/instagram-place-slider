import axios from 'axios';

const http = axios.create({
    timeout: 90000,
    responsType: 'json',
});

export default http;
