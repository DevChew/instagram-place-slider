import settings from '../../settings.json';

const getImageSrcByMaxSizeFromResourcesArray = (resources, maxSize) => {
    const filterOversizeImages = resources.filter(({ config_height }) => config_height <= maxSize);
    const filteredImagesCount = filterOversizeImages.length;
    const closestSize = filteredImagesCount ? filterOversizeImages[filteredImagesCount - 1] : resources[0];
    return closestSize.src;
};

const getPostDataFromAllPostsListSingleElement = single => (
    {
        id: single.node.shortcode,
        date: single.node.taken_at_timestamp * 1000,
        image: getImageSrcByMaxSizeFromResourcesArray(single.node.thumbnail_resources, settings.maxImageHeight),
        caption: single.node.edge_media_to_caption.edges['0'].node.text,
        likes: single.node.edge_media_preview_like.count,
        isVideo: single.node.is_video,
    }
);

export const getPostsIdFromResponse = response => response.data.graphql.hashtag.edge_hashtag_to_media.edges.map(getPostDataFromAllPostsListSingleElement);

export const getPostDataFreomResponse = (response) => {
    const data = response.data.graphql.shortcode_media;

    const id = data.shortcode;
    const image = getImageSrcByMaxSizeFromResourcesArray(data.display_resources, settings.maxImageHeight);
    const caption = data.edge_media_to_caption.edges['0'].node.text;
    const likes = data.edge_media_preview_like.count;
    const date = data.taken_at_timestamp * 1000;
    const userName = data.owner.full_name;
    const userPhoto = data.owner.profile_pic_url;
    const userLink = '';
    return {
        id,
        image,
        caption,
        likes,
        date,
        userName,
        userPhoto,
        userLink,
    };
};

export default {};
