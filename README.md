# Instagram Slider in electron

this application is build on top of electron framework and react + redux

## Install and run
```
npm i
npm run start
```
## features

* Scrap recent posts from instagram place
* preload images and store in localstorage
* store latest posts info in localsotare 
* auto remove window on system sleep and restore it on ware up
* cool css animations

# todo
* add simple clock in top left corner
* add settings menu (consider create it in separate window)
* add audio control (control external audio player or create one)
* add autostart
* add simple way to close entire app